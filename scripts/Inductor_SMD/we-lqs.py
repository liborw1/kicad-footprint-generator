# https://www.we-online.com/catalog/en/WE-LQS

inductors = [

# Dimensiton
# W is width in the on axis given by the two pads
# L is perpendicular to the W

#name,    L,   W, pad-w, pad-gap, pad-h
[8065,  8.0, 8.0,   2.4,     3.8,  7.5],
[8040,  8.0, 8.0,   2.4,     3.8,  7.5],
[6045,  6.0, 6.0,   1.8,     2.8,  5.7],
[6028,  6.0, 6.0,   1.8,     2.8,  5.7],
[5040,  5.0, 5.0,  1.55,     2.3,  4.5],
[5020,  5.0, 5.0,   1.6,     2.3,  4.5],
[4025,  4.0, 4.0,   1.5,    1.55,  4.4],
[4018,  4.0, 4.0,   1.5,    1.55,  3.6],
[4012,  4.0, 4.0,   1.5,    1.55,  3.6],
[3015,  3.0, 3.0,   1.0,     1.4,  2.7],
[3012,  3.0, 3.0,   1.4,     1.4,  3.3],
[2512,  2.0, 2.5,   1.1,     0.8,  2.0],
[2010,  2.0, 1.6,  0.82,     0.7,  1.2],
]

import sys
import os

output_dir = os.getcwd()

#if specified as an argument, extract the target directory for output footprints
if len(sys.argv) > 1:
    out_dir = sys.argv[1]

    if os.path.isabs(out_dir) and os.path.isdir(out_dir):
        output_dir = out_dir
    else:
        out_dir = os.path.join(os.getcwd(),out_dir)
        if os.path.isdir(out_dir):
            output_dir = out_dir

if output_dir and not output_dir.endswith(os.sep):
    output_dir += os.sep

#import KicadModTree files
from KicadModTree import Footprint, Pad, Text, RectLine, PolygoneLine, Model, KicadFileHandler

prefix = "L_"
part = "Wurth_LQS-{pn}"
dims = "{l:0.1f}mmx{w:0.1f}mm"

desc = "Inductor, Wurth Elektronik, LQS-{pn}"
tags = "inductor we wurth lqs smd"

for inductor in inductors:
    name,l,w,x,g,y = inductor

    fp_name = prefix + part.format(pn=str(name))

    fp = Footprint(fp_name)

    description = desc.format(pn = part.format(pn=str(name))) + ", " + dims.format(l=l,w=w)

    fp.setTags(tags)
    fp.setAttribute("smd")
    fp.setDescription(description)

    # set general values
    fp.append(Text(type='reference', text='REF**', at=[0,-w/2 - 1], layer='F.SilkS'))
    fp.append(Text(type='value', text=fp_name, at=[0,w/2 + 1.5], layer='F.Fab'))

    #add inductor outline
    fp.append(RectLine(start=[-l/2,-w/2],end=[l/2,w/2],layer='F.Fab',width=0.15))

    #calculate pad center
    #pad-width pw
    pw = x
    c = g/2 + pw/2

    layers = ["F.Cu","F.Paste","F.Mask"]

    #add pads
    fp.append(Pad(number=1,at=[-c,0],layers=layers,shape=Pad.SHAPE_RECT,type=Pad.TYPE_SMT,size=[pw,y]))
    fp.append(Pad(number=2,at=[c,0],layers=layers,shape=Pad.SHAPE_RECT,type=Pad.TYPE_SMT,size=[pw,y]))

    #add inductor courtyard
    cx = c + pw/2
    cy = w / 2

    fp.append(RectLine(start=[-cx,-cy],end=[cx,cy],offset=0.3,width=0.05,grid=0.05,layer="F.CrtYd"))

    offset = 0.1
    ly = y/2 + 4 * offset

    # fix for packages that are smaller that their pad
    if (w / 2) < (y / 2):
        x1 = g / 2 - offset
    else:
        x1 = l / 2 + offset

    y1 = w / 2 + offset

    top = [
            {'x': -x1, 'y': y1},
            {'x':  x1, 'y': y1},
    ]

    fp.append(PolygoneLine(polygone=top))
    fp.append(PolygoneLine(polygone=top,y_mirror=0))

    #Add a model
    fp.append(Model(filename="Inductors_SMD.3dshapes/" + fp_name + ".stp"))

    #filename
    filename = output_dir + fp_name + ".kicad_mod"

    file_handler = KicadFileHandler(fp)
    file_handler.writeFile(filename)

